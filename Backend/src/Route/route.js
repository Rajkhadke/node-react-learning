

const express = require ("express");
const router=express.Router()
const model = require('../model/model')
const bcrypt =require('bcrypt')
const jwt =require('jsonwebtoken')

router.post("/register", async(req,res)=>{
    try{
      
    let data=req.body;

    let {name,phone,email,password}=data
   if(!phone || !name || !email|| !password){
    return res.status(400).json({error:"Plz filled the all fild"})
    }
    let existingmail = await model.findOne({email:email})
    let existingMobile = await model.findOne({phone:phone})
    if(existingmail || existingMobile){
        return res.status(409).send({message : 'already exist'})
    }
    password=await bcrypt.hash(password,10)

    let userDetails={name,phone,email,password}

    const user= await model.create(userDetails);
     
    return res.status(201).send({message:'Account registered ',data:user})

    }catch(error){
       return res.send(error)
    }
})

router.post('/login' , async(req,res)=>{
    try {

        let data=req.body;
        const {email,password}=data
        let userEmail = await model.findOne({email:email})
        if(! userEmail ){
            return res.status(409).send({message : 'user not found'})
        } 
        const validPassword = await bcrypt.compare(password, userEmail.password);
        if (!validPassword) {
            return res.status(401).send({message: "inValid password" });
        }
        const token =jwt.sign({userId:userEmail._id, email:email},"thisIsSecretKey")   
        return res.status(200).send({token :token})
        
    } catch (error) {
        return res.send(error)
    }
})
module.exports=router