import React from 'react'
import { ErrorMessage } from 'formik'
const Errmsg = ({name}) => {
  return (
    <div className='Err'>
      <ErrorMessage  name={name}/>
      </div>
  )
}

export default Errmsg
