import React from "react";
import { Formik,Form, Field} from "formik";

import * as yup from 'yup';
import Errmsg from './Errmsg';

export default function Login() {

 
  const validate=yup.object({
    Email:yup.string()
    .required('Email is Required'),
    password:yup.string()
    .required('Password is Required'),

  });
 

  return (
    <div>
      <h1>Login here</h1>
      <Formik initialValues={{Email:"",password:''}}
      validationSchema={validate}
      onSubmit={(values) => {
       console.log(values)
     }}     >
        <Form>

     <Field  className="FieldBox" id="Email" name="Email" type="Email" placeholder="Your Email"/>
     <Errmsg name="Email" />
    <Field  className="FieldBox" id="password" name="password" type="Password" placeholder="Your Password"/>
    <Errmsg name="password" /> 
    <button className="appbtn" type="Submit"  >  LogIn </button>

        </Form>
      </Formik>

      
    </div>
  );
}
