import React from "react";
import { Link } from "react-router-dom";

export default function Dashboard() {
  return (
    <div>
      
      <ul className="nav-ul">
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/LogIn">LogIn</Link>
        </li>
        <li>
          <Link to="/Register">Register</Link>
        </li>

        <div className='Footer'>

        <li className='block'>
          <Link to="/contact">Contact</Link>
        </li>
        <li className='block'>
          <Link to="/about">About</Link>
        </li>

      
      </div>
</ul>



    </div>
  );
}
