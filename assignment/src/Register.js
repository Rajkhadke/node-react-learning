import React, { useState } from 'react';
import { Formik,Form, Field} from "formik";
import * as yup from 'yup';
import Errmsg from './Errmsg';
import Axios from "axios";


const Register = () => {

  const validate=yup.object({
    Email:yup.string()
    .required('Email is Required'),
    password:yup.string()
    .required('Password is Required'),
    Name:yup.string()
    .required('Name is Required'),
    Number:yup.string()
    .required('Number is Required'),

  });

  const [user]=useState({
    Email:"",password:'',Name:'',Number:''
  
  })

// const postdata=(values) =>{
//   const url="http://localhost:5000/register";
//   console.log(user)
//   const newUser={name:values.Name,phone:values.Number,email:values.Email,password:values.password}
//   Axios.post(url,newUser)
//   .then((res) =>{
//     console.log(res)
    
//    })
// }


  return (
    <>
    
       <h1>Register</h1>
       <Formik initialValues={{Email:"",password:'',Name:'',Number:''}} 
         validationSchema={validate}
         onSubmit={(values) => {
          console.log(values)
          const url="http://localhost:5000/register";
          console.log(user)
          const newUser={name:values.Name,phone:values.Number,email:values.Email,password:values.password}
          Axios.post(url,newUser)
          .then((res) =>{
            console.log(res)
            
          })
        }}   

        // onSubmit={postdata(values)}
       >
   <Form>
     
    <Field  className="FieldBox" id="Email" name="Email" type="Email" placeholder="Your Email"  />
    <Errmsg name="Email" />
    
    <Field  className="FieldBox" id="password" name="password" type="Password" placeholder="Your Password"/>
    <Errmsg name="password" />
    
    <Field  className="FieldBox" id="Name" name="Name" type="text" placeholder="Your Name"/>
    <Errmsg name="Name" />
    
    <Field  className="FieldBox" id="Number" name="Number" type="Number" placeholder="Your Number"/>
    <Errmsg name="Number" />
    <button className="appbtn" type="Submit"  >Submit</button>

    </Form>
       </Formik>
    
    </>
  )
}

export default Register;
